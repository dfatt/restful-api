<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cinema extends MY_Controller
{
    /**
     * Вывод списка фильмов в кинотеатре, также можно фильтровать по залам
     *
     * @param $cinema_id
     * @param null $hall_id
     */
    public function schedule_get($cinema_id, $hall_id = null)
    {
        $films = $this->cinema_model->get_schedules_by_cinema_id($cinema_id, $hall_id)->result(false, 'sessions');

        // Проверяем, есть ли записи, если нет - показываем сообщение о пустом результате
        if (null != $films)
            $this->response($films, 200);
        else
            $this->response(array('error' => $this->lang->line('empty_result')), 404);
    }
}