<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends MY_Controller
{
    /**
     * Покупка билета
     *
     * @param $session_id
     * @param $place
     */
    public function buy_post($session_id, $place)
    {
        if ($code = $this->cinema_model->buy_ticket($session_id, $place))
            $this->response('Билет успешно куплен. Его код на случай отмены заказа: ' . $code, 200);
        else
            $this->response(array('error' => $this->lang->line('error_by')), 400);
    }

    /**
     * Отмена покупки билета
     *
     * @param $code
     */
    public function reject_post($code)
    {
        $message = array('id' => $this->get('id'), 'name' => $this->post('name'), 'email' => $this->post('email'), 'message' => 'ADDED!');

        $this->response($message, 200);
    }
}