<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Film extends MY_Controller
{
    /**
     * Вывод списка кинотеатров в которых идёт фильм
     *
     * @param $film_id
     */
    public function schedule_get($film_id)
    {
        $films = $this->cinema_model->get_schedule_by_film_id($film_id)->result(false, 'sessions');

        // Проверяем, есть ли записи, если нет - показываем сообщение о пустом результате
        if (null != $films)
            $this->response($films, 200);
        else
            $this->response(array('error' => $this->lang->line('empty_result')), 404);
    }
}