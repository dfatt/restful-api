<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller
{
    public function buy()
    {
        $post = $this->input->post();

        if ($code = $this->cinema_model->buy_ticket($post['session_id'], $post['place_id']))
            print_r('Билет успешно куплен. Его код на случай отмены заказа: ' . $code);
        else
            print_r('Произошла ошибка при покупке.');
    }

    public function reject()
    {
        $post = $this->input->post();

        if ($this->cinema_model->reject_ticket($post['code']))
            print_r('Заказ успешно отменён');
        else
            print_r('Произошла ошибка при отмене заказа.');
    }
}