<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Session extends MY_Controller
{
    /**
     * Список свободных мест
     *
     * @param $place
     */
    public function places_get($place)
    {
        $tickets = $this->cinema_model->get_places($place)->result(false, 'tickets');

        // Проверяем, есть ли записи, если нет - показываем сообщение о пустом результате
        if (null != $tickets)
            $this->response($tickets, 200);
        else
            $this->response(array('error' => $this->lang->line('empty_result')), 404);
    }
}