<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>TicketBuy — Покуйпайте билеты On-line, не выходя из дома!</title>

<style type="text/css">

body {
 background-color: #fff;
 margin: 40px;
 font-family: Arial, Tahoma, Sans-serif;
 font-size: 14px;
 color: #4F5155;
}

a {
 color: #003399;
 background-color: transparent;
 font-weight: normal;
 line-height: 25px
}

h1 {
 color: #444;
 background-color: transparent;
 border-bottom: 1px solid #D0D0D0;
 font-size: 16px;
 font-weight: bold;
 margin: 24px 0 2px 0;
 padding: 5px 0 6px 0;
}

code {
 font-family: Monaco, Verdana, Sans-serif;
 font-size: 12px;
 background-color: #f9f9f9;
 border: 1px solid #D0D0D0;
 color: #002166;
 display: block;
 margin: 14px 0 14px 0;
 padding: 12px 10px 12px 10px;
}

label {
    display: block;
    margin-bottom: 10px;
}

/*** Стили приложения ***/
.dialog {
    position: fixed;
    z-index: 100;
    width: 480px;
    height: 250px;
    left: 31%;
    background: #fff;
    padding: 20px;
    box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.4);
    border-radius: 5px;
}
.buy.dialog {
    display: none;
    margin-bottom: 15px;
}
.reject.dialog {
    display: none;
}
.dialog h1 { margin: -14px 0 14px 0; }
.dialog .close {
    float: right;
    top: -9px;
    left: -2px;
    position: relative;
    font-size: 27px;
    text-decoration: none;
}

.overlay {
    display: none;
    width: 100%;
    height: 100%;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 99;
    background: #000;
    opacity: 0.55;
    filter: alpha(opacity= 55);
    zoom: 1;
}

.info {
    background-color:#dff0d8;
    padding: 15px;
}
</style>
</head>
<body>

<h1>Меню</h1>


1) <a href="#" onclick="$('.overlay, .buy').fadeToggle();">Купить билет</a>
<div class="buy dialog">
    <a href="#" onclick="closeDlg();" class="close">&#735;</a>
    <h1>Купить билет</h1>

    <form action="/post/buy" method="post">
        <p class="info">
            Тестовые данные, в поле <i>сеанс</i> указать <b>3</b>. В поле <i>место</i> указать <b>12</b>
        </p>
        <label>
            Сеанс:
            <input type="text" name="session_id" />
        </label>

        <label>
            Место:
            <input type="text" name="place_id" />
        </label>

        <input type="submit" name="buy_btn" value="Купить"/>
    </form>
</div>

<br />

2) <a href="#" onclick="$('.overlay, .reject').fadeToggle();">Отменить покупку билета</a>
<div class="reject dialog">
    <a href="#" onclick="closeDlg();" class="close">&#735;</a>
    <h1>Отменить покупку</h1>
    <form action="/post/reject" method="post">
        <p class="info">
            Тестовые данные, в поле <i>код</i> указать <b>gMNrDqe3</b>.
        </p>
        <label>
            Код:
            <input type="text" name="code" />
        </label>

        <input type="submit" name="reject_btn" value="Отменить покупку"/>
    </form>
</div>

<script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<div class="overlay"></div>

<script>
    $('.overlay').click(function() {
        closeDlg();
    });

    function closeDlg()
    {
        $('.dialog, .overlay').fadeOut();
    }
</script>

<p><br />Page rendered in {elapsed_time} seconds</p>
</body>
</html>