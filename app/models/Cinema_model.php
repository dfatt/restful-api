<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Модель кинотеатра
 * Class Cinema_model
 */
class Cinema_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Вывод списка фильмов, которые проходят в кинотеатре
     *
     * @param $cinema_id
     * @param null $hall
     */
    public function get_schedules_by_cinema_id($cinema_id, $hall = null)
    {
        // Если идёт выборка с указанием зала, добавляем к нашему фильтру условие
        if (null != $hall) {
            $query = $this->db->where('h.id', $hall);
        }

        $query = $this->db->select('f.*, s.*, c.name cinema_name')
                          ->from('sessions s')
                          ->where('c.id', $cinema_id)
                          ->join('films f', 's.id_film = f.id', 'left')
                          ->join('halls h', 's.id_hall = h.id', 'left')
                          ->join('cinemas c', 'h.id_cinema = c.id', 'left')
                          ->get();

        return $query;
    }

    /**
     * Список кинотеатров в которых идёт конкретный фильм
     *
     * @param $film_id
     * @return mixed
     */
    public function get_schedule_by_film_id($film_id)
    {
        return $this->db->select('f.*, s.*, c.name cinema_name')
                        ->from('sessions s')
                        ->where('f.id', $film_id)
                        ->join('films f', 's.id_film = f.id', 'left')
                        ->join('halls h', 's.id_hall = h.id', 'left')
                        ->join('cinemas c', 'h.id_cinema = c.id', 'left')
                        ->get();
    }

    /**
     * Список свободных мест
     *
     * @param $place
     */
    public function get_places($place)
    {
        return $this->db->select('*')
                        ->from('tickets t')
                        ->where('s.id', $place)
                        ->where('t.code is null', null, false)
                        ->join('sessions s', 's.id = t.id_session', 'left')
                        ->get();
    }

    /**
     * Покупка билета
     *
     * @param $session_id
     * @param $place
     * @return int|string
     */
    public function buy_ticket($session_id, $place)
    {
        $this->load->helper('string');

        $code = random_string('alnum', 8);
        $where = array(
            'id_session' => $session_id ,
            'id_place' => $place,
            'code' => null
        );

        $result = $this->db->select('*')
                 ->from('tickets t')
                 ->where($where);

        // Если результат равен одному, значит билет такой есть.
        if ($this->db->count_all_results() == 1)
        {
            $this->db->where($where)
                     ->update('tickets', array('code' => $code));

            return $code;
        }
    }

    /**
     * Отмена покупки
     *
     * @param $code
     * @return bool
     */
    public function reject_ticket($code)
    {
        $this->db->select('*')
                 ->from('tickets t')
                 ->where('t.code is not null', null, false);

        // Если результат равен одному, значит такой заказ был проведён
        if ($this->db->count_all_results() == 1) {
            $this->db->where('code', $code)
                     ->update('tickets', array('code' => null));

            return true;
        }
    }
}