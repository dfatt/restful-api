# ************************************************************
# Sequel Pro SQL dump
# Версия 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Адрес: 127.0.0.1 (MySQL 5.6.15)
# Схема: restful_api
# Время создания: 2014-04-28 10:39:58 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы cinemas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cinemas`;

CREATE TABLE `cinemas` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(115) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cinemas` WRITE;
/*!40000 ALTER TABLE `cinemas` DISABLE KEYS */;

INSERT INTO `cinemas` (`id`, `name`, `address`)
VALUES
	(1,'Киномакс - Урал','г. Челябинск, ул. Воровского, 6'),
	(2,'Синема Парк','г. Челябинск, ул. Артиллерийская, 136');

/*!40000 ALTER TABLE `cinemas` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы films
# ------------------------------------------------------------

DROP TABLE IF EXISTS `films`;

CREATE TABLE `films` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) DEFAULT NULL,
  `picture` varchar(115) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `films` WRITE;
/*!40000 ALTER TABLE `films` DISABLE KEYS */;

INSERT INTO `films` (`id`, `name`, `picture`)
VALUES
	(1,'Ной','noah.jpg'),
	(2,'47 ронинов','47_ronin.jpg');

/*!40000 ALTER TABLE `films` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы halls
# ------------------------------------------------------------

DROP TABLE IF EXISTS `halls`;

CREATE TABLE `halls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_cinema` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CINEMA` (`id_cinema`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `halls` WRITE;
/*!40000 ALTER TABLE `halls` DISABLE KEYS */;

INSERT INTO `halls` (`id`, `id_cinema`)
VALUES
	(1,1),
	(2,1),
	(3,1),
	(5,1),
	(4,2),
	(6,2),
	(7,2);

/*!40000 ALTER TABLE `halls` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы places
# ------------------------------------------------------------

DROP TABLE IF EXISTS `places`;

CREATE TABLE `places` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_hall` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_HALL` (`id_hall`),
  CONSTRAINT `FK_HALL` FOREIGN KEY (`id_hall`) REFERENCES `halls` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `places` WRITE;
/*!40000 ALTER TABLE `places` DISABLE KEYS */;

INSERT INTO `places` (`id`, `id_hall`)
VALUES
	(12,1),
	(13,1),
	(14,1),
	(15,1),
	(16,1),
	(17,1),
	(18,1),
	(19,1),
	(20,1),
	(21,1),
	(22,1),
	(23,1),
	(24,1),
	(25,1),
	(26,2),
	(27,2),
	(28,2),
	(29,2),
	(30,2),
	(31,2),
	(32,2),
	(33,2),
	(34,2),
	(35,2),
	(36,2),
	(37,2),
	(38,2);

/*!40000 ALTER TABLE `places` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sessions`;

CREATE TABLE `sessions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_film` int(11) unsigned NOT NULL,
  `id_hall` int(11) unsigned NOT NULL,
  `price` float DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_FILM` (`id_film`),
  KEY `FK_HALL1` (`id_hall`),
  CONSTRAINT `FK_HALL1` FOREIGN KEY (`id_hall`) REFERENCES `halls` (`id`),
  CONSTRAINT `FK_FILM` FOREIGN KEY (`id_film`) REFERENCES `films` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;

INSERT INTO `sessions` (`id`, `id_film`, `id_hall`, `price`, `time`)
VALUES
	(3,1,1,500,'2014-08-20 15:02:00'),
	(4,1,2,650,'2014-08-20 15:02:00');

/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы tickets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tickets`;

CREATE TABLE `tickets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_session` int(11) unsigned NOT NULL,
  `id_place` int(11) unsigned NOT NULL,
  `code` varchar(115) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_SESSION` (`id_session`),
  KEY `FK_PLACE` (`id_place`),
  CONSTRAINT `FK_PLACE` FOREIGN KEY (`id_place`) REFERENCES `places` (`id`),
  CONSTRAINT `FK_SESSION` FOREIGN KEY (`id_session`) REFERENCES `sessions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tickets` WRITE;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;

INSERT INTO `tickets` (`id`, `id_session`, `id_place`, `code`)
VALUES
	(16,3,12,NULL),
	(17,3,13,'gMNrDqe3'),
	(18,3,17,NULL);

/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
